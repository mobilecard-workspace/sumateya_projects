package com.addcel.sumateya.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class MontosVO {
	private int idMonto;
	private int monto;
	public int getIdMonto() {
		return idMonto;
	}
	public void setIdMonto(int idMonto) {
		this.idMonto = idMonto;
	}
	public int getMonto() {
		return monto;
	}
	public void setMonto(int monto) {
		this.monto = monto;
	}
	
}

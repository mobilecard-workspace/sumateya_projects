package com.addcel.sumateya.model.vo;

import java.io.Serializable;

public class RespuestaPagoVO implements Serializable {

	private static final long serialVersionUID = 1401273729967480196L;
	
	private String referencia;
	private String noAutorizacion;
	private String idError;
	private String mensajeError;
	
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getNoAutorizacion() {
		return noAutorizacion;
	}
	public void setNoAutorizacion(String noAutorizacion) {
		this.noAutorizacion = noAutorizacion;
	}
	public String getIdError() {
		return idError;
	}
	public void setIdError(String idError) {
		this.idError = idError;
	}
	public String getMensajeError() {
		return mensajeError;
	}
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

}

package com.addcel.sumateya.model.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.addcel.sumateya.model.vo.MontosVO;
import com.addcel.sumateya.model.vo.request.DatosPago;

public interface SumateYaMapper {
	
	public List<MontosVO> listaMontos(@Param(value = "idProveedor") String idProveedor);

	public String getFechaActual();

	public int difFechaMin(String fechaToken);
	
	public int insertSumateYaDetalle(DatosPago datosPago);
	
	public int updateSumateYaDetalle(DatosPago datosPago);
}

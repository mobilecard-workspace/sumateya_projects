package com.addcel.sumateya.model.vo.request;

import java.io.Serializable;

public class RequestCatalogos implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String proveedor;

	public String getProveedor() {
		return proveedor;
	}

	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}
}

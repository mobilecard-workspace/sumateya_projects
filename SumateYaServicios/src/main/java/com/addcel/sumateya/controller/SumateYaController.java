package com.addcel.sumateya.controller;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.addcel.sumateya.services.SumateYaPagoAMEX;
import com.addcel.sumateya.services.SumateYaPagoService;
import com.addcel.sumateya.services.SumateYaService;

/**
 * Handles requests for the application home page.
 */
@Controller
public class SumateYaController {
	
	private static final Logger logger = LoggerFactory.getLogger(SumateYaController.class);
	@Autowired
	private SumateYaService dfService;
	@Autowired
	private SumateYaPagoService dfpService;
	@Autowired
	private SumateYaPagoAMEX amexService;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);				
		return "home";
	}

	@RequestMapping(value = "/getMontos")
	public @ResponseBody String listaTiendas(@RequestParam("json") String data) {		
		return dfService.listaMontos(data);
	}

	
	@RequestMapping(value = "/getToken")
	public @ResponseBody String getToken(@RequestParam("json") String data) {		
		return dfService.generaToken(data);
	}
	
	@RequestMapping(value = "/pago-visa")
	public @ResponseBody String datosPago(@RequestParam("json") String jsonEnc) {	
		return dfpService.procesaPago(jsonEnc);	
	}
	
//	@RequestMapping(value = "/pago-amex") //*******************************************************
//	public @ResponseBody String pagoAmex(@RequestParam("json") String data) {
//		return amexService.procesaPago(data);
//	}
	
	
}
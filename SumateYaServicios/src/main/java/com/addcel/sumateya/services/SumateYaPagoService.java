package com.addcel.sumateya.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.sumateya.model.mapper.SumateYaMapper;
import com.addcel.sumateya.model.vo.ProsaPagoVO;
import com.addcel.sumateya.model.vo.RespuestaPagoVO;
import com.addcel.sumateya.model.vo.request.DatosPago;
import com.addcel.sumateya.utils.AddCelGenericMail;
import com.addcel.sumateya.utils.UtilsService;
import com.addcel.utils.AddcelCrypto;

@Service
public class SumateYaPagoService {
	private static final Logger logger = LoggerFactory.getLogger(SumateYaPagoService.class);
	private static final String URL_AUT_PROSA = "http://localhost:8080/ProsaWeb/ProsaAuth?";
	private static final String URL_REV_PROSA = "http://localhost:8080/ProsaWeb/ProsaRev?";
	
//	private static final String URL_AUT_PROSA = "http://50.57.192.213:8080/ProsaWeb/ProsaAuth";
//	private static final String URL_REV_PROSA = "http://50.57.192.213:8080/ProsaWeb/ProsaRev";
	
	private static final String patron = "ddhhmmss";
	private static final String patronCom = "yyyy-MM-dd hh:mm:ss";
	private static final SimpleDateFormat formatoCom = new SimpleDateFormat(patronCom);
	private static final SimpleDateFormat formato = new SimpleDateFormat(patron);
	
	@Autowired
	private UtilsService utilService;	
	@Autowired
	private SumateYaMapper mapperSMY;
		
	public String procesaPago(String jsonEnc){		
		// prueba
		DatosPago datosPago = null;
		String json = null;
		ProsaPagoVO prosaPagoVO = null;
//		long idBitacora = 0;
//		String token = null;
		
		try{
//			token=mapper.getFechaActual();
			json=AddcelCrypto.decryptSensitive(jsonEnc);		
			logger.debug("jsondatospago: {}",json);
			datosPago = (DatosPago) utilService.jsonToObject(json, DatosPago.class);
			if(datosPago.getToken() == null){
				json = "{\"idError\":1,\"mensajeError\":\"El parametro TOKEN no puede ser NULL\"}";
				logger.error("El parametro TOKEN no puede ser NULL");
			}else{
				datosPago.setToken(AddcelCrypto.decryptHard(datosPago.getToken()));
				logger.info("token ==> {}",datosPago.getToken());
				
//				if((mapperSMY.difFechaMin(datosPago.getToken())) < 1){
//				if(true){
					//Insetar Bitacoras Prosa y detalle						
					logger.info("****************** MXD ******************");
//					datosPago.setIdBitacora( mapperSMY.insertSumateYaDetalle(datosPago));
					mapperSMY.insertSumateYaDetalle(datosPago);
					
					try{
						StringBuffer data = new StringBuffer() 
								.append( "card="      ).append( URLEncoder.encode(datosPago.getTarjeta(), "UTF-8") )
								.append( "&vigencia=" ).append( URLEncoder.encode(datosPago.getVigencia(), "UTF-8") )
								.append( "&nombre="   ).append( URLEncoder.encode(datosPago.getNombre(), "UTF-8") )
								.append( "&cvv2="     ).append( URLEncoder.encode(datosPago.getCvv2(), "UTF-8") )
								.append( "&monto="    ).append( URLEncoder.encode(datosPago.getMonto(), "UTF-8") )
								.append( "&afiliacion=" ).append( URLEncoder.encode(datosPago.getVigencia(), "UTF-8") )
								.append( "&moneda="   ).append( URLEncoder.encode("MXN", "UTF-8")) ;
								
								 
						
						logger.info("Envío de datos PROSA: {}", data);
						
						URL url = new URL(URL_AUT_PROSA);
						URLConnection urlConnection = url.openConnection();
						
						urlConnection.setDoOutput(true);
						
						OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
						wr.write(data.toString());
						wr.flush();
						logger.info("Datos enviados, esperando respuesta de PROSA");
						
						BufferedReader rd = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
						String line;
						StringBuilder sb = new StringBuilder();
						
						while ((line = rd.readLine()) != null) {
							sb.append(line);
						}
						
						wr.close();
						rd.close();
						
						prosaPagoVO = (ProsaPagoVO) utilService.jsonToObject(sb.toString(), ProsaPagoVO.class);
						
						if(prosaPagoVO != null){
							datosPago.setNoAutorizacion(prosaPagoVO.getAutorizacion());
							datosPago.setReferencia(prosaPagoVO.getTransactionId());
							datosPago.setFecha(formatoCom.format(new Date()));
//							datosPago.setTransaccion(prosaPagoVO.getTransaccion());
//							datosPago.setMsg(prosaPagoVO.getMsg());
							
							if(prosaPagoVO.isAuthorized()){
								datosPago.setStatus(1);
								json = "{\"idError\":0,\"mensajeError\":\"El pago fue exitoso.\",\"referencia\":" + prosaPagoVO.getTransactionId() + 
										",\"autorizacion\":" + prosaPagoVO.getAutorizacion() +"}";
								
								AddCelGenericMail.sendMail(utilService.objectToJson(AddCelGenericMail.generatedMail(datosPago)));
								
							}else if(prosaPagoVO.isRejected()){
								datosPago.setStatus(3);
								json = "{\"idError\":4,\"mensajeError\":\"El pago fue rechazado.\",\"referencia\":" + prosaPagoVO.getTransactionId() + "}";
								
							}else if(prosaPagoVO.isProsaError()){
								datosPago.setStatus(2);
								json = "{\"idError\":5,\"mensajeError\":\"Ocurrio un error durante el pago Banco.\",\"referencia\":" + prosaPagoVO.getTransactionId() + "}";
							}
							
							mapperSMY.updateSumateYaDetalle(datosPago);
						}
						
					}catch(Exception e){
						logger.error("Ocurrio un error durante el pago al banco:", e);
						datosPago.setStatus(2);
						json = "{\"idError\":6,\"mensajeError\":\"Ocurrio un error durante el pago.\"}";
						mapperSMY.updateSumateYaDetalle(datosPago);
					}
					
//				}else{
//					json = "{\"idError\":2,\"mensajeError\":\"La Transacción ya no es válida\"}";
//					logger.error("La Transacción ya no es válida");
//				}
				
			}
		}catch(Exception e){
			logger.error("Ocurrio un error:", e);
			datosPago.setMensajeError("Ocurrio un error: " + e.getMessage());
			datosPago.setStatus(2);
			mapperSMY.updateSumateYaDetalle(datosPago);
			json = "{\"idError\":3,\"mensajeError\":\"Ocurrio un error: " + e.getMessage() + "\"}";
		}
		json = AddcelCrypto.encryptSensitive(formato.format(new Date()),json);
		return json;
	}	
	
}


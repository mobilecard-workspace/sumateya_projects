package com.addcel.sumateya.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class SumateYaPagoAMEX {
	
	private static final Logger logger = LoggerFactory.getLogger(SumateYaPagoService.class);
	private static final String urlStringAMEX = "http://localhost:8080/AmexWeb/AmexAuthorization";
	
//	@Autowired
//	private BitacorasMapper mapper;
//	@Autowired
//	private UtilsService utils;
//	@Autowired
//	private SumateYaMapper mapperDF;
//	
//	private DuttyFreeDetalleVO detalle;
//	private TBitacoraVO b;
	
//	public String procesaPago(String dataEncryp) {
//		RespuestaPagoVO respuestaAmex = new RespuestaPagoVO();
//		List<DetalleVO> detalleVO = null;
//		TransactionProcomVO tp = new TransactionProcomVO();
//		
//		DatosPago pagoAmex = (DatosPago) utils.jsonToObject(AddcelCrypto.decryptSensitive(dataEncryp), DatosPago.class);
////		pagoAmex.setToken(AddcelCrypto.decryptHard(pagoAmex.getToken()));
//		
////		logger.info("Token --> {}", pagoAmex.getToken());
//		
////		int dif = mapperDF.difFechaMin(pagoAmex.getToken());
////		if (dif < 1) {
//			guardarBitacora(pagoAmex);
//			guardarBitacoraDetalle(pagoAmex, b.getIdBitacora());
//			
//			try {
//				String producto = pagoAmex.getMoneda().equals("1") ? "DutyFree1" : "DutyFree2";
//				
//				String data = "tarjeta=" + URLEncoder.encode(pagoAmex.getTarjeta(), "UTF-8") +
//						"&vigencia=" + URLEncoder.encode(pagoAmex.getVigencia(), "UTF-8") +
//						"&monto=" + URLEncoder.encode(String.valueOf(pagoAmex.getTotal()), "UTF-8") +
//						"&cid=" + URLEncoder.encode(pagoAmex.getCvv2(), "UTF-8") + 
//						"&direccion=" + URLEncoder.encode(pagoAmex.getDireccion(), "UTF-8") +
//						"&cp=" + URLEncoder.encode(pagoAmex.getCp(), "UTF-8") +
//						"&producto=" + URLEncoder.encode(producto, "UTF-8");
//				
//				logger.info("Env�o de datos AMEX: {}", data);
//				
//				URL url = new URL(urlStringAMEX);
//				URLConnection urlConnection = url.openConnection();
//				
//				urlConnection.setDoOutput(true);
//				
//				OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
//				wr.write(data);
//				wr.flush();
//				logger.info("Datos enviados, esperando respuesta de AMEX");
//				
//				BufferedReader rd = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
//				String line;
//				StringBuilder sb = new StringBuilder();
//				
//				while ((line = rd.readLine()) != null) {
//					sb.append(line);
//				}
//				
//				wr.close();
//				rd.close();
//				
//				respuestaAmex = (RespuestaPagoVO) utils.jsonToObject(sb.toString(), RespuestaPagoVO.class);
//				
//				//*******************************************
//				String patron = "000000";
//		    	String patron2 = "000000000000";
//		    	DecimalFormat formato = new DecimalFormat(patron);
//		    	DecimalFormat formato2 = new DecimalFormat(patron2);
//				Random  rnd = new Random();
//		    	
//		    	int aut = (int)(rnd.nextDouble() * 900000);
//		    	long ref = (long)(rnd.nextDouble() * 900000000);
//		    	
////		    	tp.setEmAuth(formato.format(aut));
//		    	respuestaAmex.setTransaction(formato2.format(ref));
//		    	//*******************************************
//				
//				logger.info("Respuesta: {}", sb.toString());
//				logger.info("*****************CODIGO AMEX {}", respuestaAmex.getCode());
//				if (respuestaAmex.getCode().equals("000")) { //Pago realizado con �xito
//					logger.info("Pago realizado con �xito.");
//					b.setBitStatus(1);
//					b.setBitCargo(String.valueOf(pagoAmex.getTotal()));
//					b.setBitConcepto("EXITO PAGO AMEX FREE DUTTY");
//					b.setBitNoAutorizacion(respuestaAmex.getTransaction());
//					actualizarBitacora();
//					actualizarBitacoraDetalle(1);
//					
//					detalleVO = mapperDF.getDetalle(null, null, null, null, null, null, String.valueOf(b.getIdBitacora()));
//					if(detalleVO != null && detalleVO.size() >0){
//						tp.setEmTotal(Utilerias.formatoImporteMon( pagoAmex.getTotal()));
//						tp.setProducto(pagoAmex.getDescProducto());
//						tp.setMoneda(pagoAmex.getMoneda());
//						tp.setFecha(detalleVO.get(0).getFecha());
//						tp.setEmRefNum(respuestaAmex.getTransaction());
//						tp.setEmOrderID(b.getIdBitacora() + "");
//					}else{
//						logger.info("No hay datos en la consulta de detalle");
//					}
//					
//					AddCelGenericMail.sendMail(
//							utils.objectToJson(AddCelGenericMail.generatedMail(tp, pagoAmex.getEmail())));
//				} else {
//					logger.info("El pago no se pudo realizar error {}", respuestaAmex.getDsc());
//					b.setBitStatus(2);
//					b.setBitCodigoError(Integer.parseInt(respuestaAmex.getCode()));
//
//					actualizarBitacora();
//					actualizarBitacoraDetalle(2);
//				}
//				
//			} catch (Exception ex) {
//				logger.error("Error al procesar pago AMEX: {}", ex);
//				logger.info("El pago no se pudo realizar error {}", ex);
//				b.setBitStatus(2);
//				b.setBitCodigoError(2);
//				respuestaAmex.setCode("002");
//				respuestaAmex.setDsc("Ocurrio un error durante la transacci�n: " + ex.getMessage());
//				actualizarBitacora();
//				actualizarBitacoraDetalle(2);
//			}
////		} else {
////			respuestaAmex.setCode("001");
////			respuestaAmex.setDsc("La transacci�n no es v�lida.");
////		}
//		
//		return AddcelCrypto.encryptSensitive("12345678", utils.objectToJson(respuestaAmex));
//	}
//	
//	private int guardarBitacora(DatosPago pago) {
//		logger.info("Guardando bitacora");
//		b = new TBitacoraVO();
//		
//		b.setIdUsuario(pago.getIdUser());
//		b.setBitConcepto("PAGO DUTTY FREE AMEX");
//		b.setBitStatus(0);
//		b.setImei(pago.getImei());
//		b.setDestino("PAGO DUTTY FREE AMEX");
//		b.setTarjetaCompra(pago.getTarjeta());
//		b.setTipo(pago.getTipo());
//		b.setSoftware(pago.getSoftware());
//		b.setModelo(pago.getModelo());
//		b.setWkey(pago.getWkey());
//		
//		return mapper.addBitacora(b);
//	}
//	
//	private int guardarBitacoraDetalle(DatosPago pago, long idBitacora) {
//		logger.info("Guardando bitacoraDetalle");
//		detalle = new DuttyFreeDetalleVO();
//		
//		detalle.setIdBitacora(idBitacora);
//		detalle.setIdUsuario(pago.getIdUser());
//		detalle.setStatus(0);
//		detalle.setDescProducto(pago.getDescProducto());
//		detalle.setTotal(pago.getTotal());
//		detalle.setMoneda(pago.getMoneda());
//		detalle.setTipoCambio(0.0);
//		
//		detalle.setNacionalidad(pago.getNacionalidad());
//		detalle.setPasaporte(pago.getPasaporte());
//		detalle.setNombre_completo(pago.getNombre_completo());
//		detalle.setFecha_nacimiento(pago.getFecha_nacimiento());
//		detalle.setNumero_vuelo(pago.getNumero_vuelo());
//		
//		return mapper.addDfDetalle(detalle);
//	}
//	
//	private int actualizarBitacora() {
//		logger.info("Actualizando bitacora");
//		return mapper.updateBitacora(b);
//	}
//	
//	private int actualizarBitacoraDetalle(int status) {
//		logger.info("Actualizando bitacora detalle");
//		return mapper.updateDfDetalle((int)b.getIdBitacora(), status);
//	}
//	
//	
////private static final String urlString = "http://127.0.0.1:8080/MailSenderAddcel/enviaCorreoAddcel";
////	
////	private static final StringBuffer HTML_DOBY = new StringBuffer()
////			.append("<div style=\"width: 680px; margin: 0 auto\"> <table> <tbody> <tr> <td>" )
////			.append("<img src=\"http://50.57.192.213:8080/MailSenderAddcel/resources/images/duttyfree/logo-la-riviera.jpg\">" )
////			.append("</td> <td></td> <td><p style=\"font-size: 18px\">La Riviera</p> <p style=\"font-size: 14px\">Duty Free</p></td> </tr> </tbody> </table> " )
////			.append("<p style=\"font-weight: bold; font-size: 18px\">Estimado Usuario.</p> <br> <br> " )
////			.append("<p>Usted ha realizado un Pago Bancario en La Riviera - Duty Free.</p>" )
////			.append("<p>Con los siguientes datos:</p> <p></p> " )
////			.append("<p>     Producto: <span style=\"font-size: 18px; font-weight: bold;\"><#PRODUCTO#></span> " )
////			.append("<p>     Monto: <span style=\"font-size: 18px; font-weight: bold;\"><#MONTO#></span></p>  " )
////			.append("<p>     Moneda: <span style=\"font-size: 18px; font-weight: bold;\"><#MONEDA#></span> </p> " )
////			.append("<p>     Referencia: <span style=\"font-size: 18px; font-weight: bold;\"><#REFE#></span> </p>  <br> <br> " )
////			.append("<p style=\"font-size: 12px\">Nota. Este correo es de car�ter informativo, no es necesario que responda al mismo.</p> </div");
////	
////	public void generatedMail(TransactionProcomVO tp,String email){
////		logger.info("Iniciando proceso de envio email: " + email);
////		
////		CorreoVO correo = new CorreoVO();
////		try{
////		
////	//		String[] attachments = {src_file};
////			String body = HTML_DOBY.toString();
////			body = body.replaceAll("<#PRODUCTO#>", tp.getProducto() != null ? tp.getProducto() : "");
////			body = body.replaceAll("<#MONTO#>", tp.getEmTotal());
////			body = body.replaceAll("<#MONEDA#>", "MXN");
////			body = body.replaceAll("<#REFE#>", tp.getEmRefNum()!= null ? tp.getEmRefNum() : "");
////			
////			
////			String from = "no-reply@addcel.com";
////			String subject = "Acuse Pago Duty Free - Referencia: " + tp.getEmRefNum();
////			String[] to = {email};
////	
////	//		correo.setAttachments(attachments);
////			correo.setBcc(new String[]{});
////			correo.setCc(new String[]{});
////			correo.setBody(body);
////			correo.setFrom(from);
////			correo.setSubject(subject);
////			correo.setTo(to);
////			
////			sendMail(correo);
////			logger.info("Fin proceso de envio email");
////		}catch(Exception e){
////			logger.error("Ocurrio un error al enviar el email ", e);
////		}
////	}
////
////	public void sendMail(CorreoVO correo) {
////		String line = null;
////		StringBuilder sb = new StringBuilder();
////		String data = null;
////		try {
////			data = utils.objectToJson(correo);
////
////			logger.info("data = " + data);
////
////			URL url = new URL(urlString);
////			logger.info("Conectando con " + urlString);
////			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
////
////			urlConnection.setDoOutput(true);
////			urlConnection.setRequestProperty("Content-Type", "application/json");
////			urlConnection.setRequestProperty("Accept", "application/json");
////			urlConnection.setRequestMethod("POST");
////
////			OutputStreamWriter writter = new OutputStreamWriter(urlConnection.getOutputStream());
////			writter.write(data);
////			writter.flush();
////
////			logger.info("Datos enviados, esperando respuesta");
////
////			BufferedReader reader = new BufferedReader(new InputStreamReader(
////					urlConnection.getInputStream()));
////
////			while ((line = reader.readLine()) != null) {
////				sb.append(line);
////			}
////
////			logger.info("Respuesta del servidor " + sb.toString());
////		} catch (Exception ex) {
////			logger.error("Error en: sendMail, al enviar el email ", ex);
////		}
////	}
}
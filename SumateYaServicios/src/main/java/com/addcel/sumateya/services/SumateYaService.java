package com.addcel.sumateya.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.sumateya.model.mapper.SumateYaMapper;
import com.addcel.sumateya.model.vo.TokenVO;
import com.addcel.sumateya.model.vo.request.RequestCatalogos;
import com.addcel.sumateya.utils.UtilsService;
import com.addcel.utils.AddcelCrypto;

@Service
public class SumateYaService {
	private static final Logger logger = LoggerFactory.getLogger(SumateYaService.class);
	
	@Autowired
	private SumateYaMapper mapper;
	@Autowired
	private UtilsService utilService;	
	
	public String listaMontos(String data) {
		RequestCatalogos rct = (RequestCatalogos) utilService.jsonToObject(AddcelCrypto.decryptHard(data), RequestCatalogos.class);
		return AddcelCrypto.encryptHard(utilService.objectToJson(mapper.listaMontos(rct.getProveedor())));
	}
	
	public String generaToken(String json){				
		TokenVO tokenVO = null;
		try{
			tokenVO = (TokenVO) utilService.jsonToObject(AddcelCrypto.decryptHard(json), TokenVO.class);
			if(!"userPrueba".equals(tokenVO.getUsuario()) || !"passwordPrueba".equals(tokenVO.getPassword())){
				json="{\"idError\":2,\"mensajeError\":\"No tiene permisos para consumir este webservice.\"}";
			}else{
				json="{\"token\":\""+AddcelCrypto.encryptHard(mapper.getFechaActual())+"\",\"idError\":0,\"mensajeError\":\"\"}";
			}
		}catch(Exception e){
			json="{\"idError\":1,\"mensajeError\":\"Ocurrio un error al generar el Token.\"}";
		}
		json = AddcelCrypto.encryptHard(json);
				
		return json;
	}

}
